# -*- coding: utf-8 -*-
"""
Created on Wed May 18 15:53:00 2022

@author: adria036

------------------------------------------------------------------------------
KB sensing potential:
    
Module 1:
    - load packages
    - read data
    - edit data - based on 1 measurement per second
    - summarize data
    
"""

#%% import packages

import os
import pandas as pd
import numpy as np
import copy
# import matplotlib.pyplot as plt
from datetime import date
from read_data_sewio import read_all_data, read_ids, combine_data_id
from scipy.signal import medfilt
import scipy
# import timeit
import barn_areas


#%% functions

def filter_barn_edges(ts,edge1,edge2):
    """
    Filters the data according to barn edges given by edge1 and edge2
    Method: puts data outside edges to nan or to closest edge if < 30cm
        if ts < edge1 : ts = nan
        if ts > edge2 : ts = nan

    Parameters
    ----------
    ts : TYPE = Pandas series (time series)
        series that edges need to be applied to to detect/modify out-of-bound =
        out of barn measurements
    edge1 : TYPE = first edge of barn
        left/lower (lowest value)
    edge2 : TYPE = second edge of barn
        right/lower (highest value)

    Returns
    -------
    ts_filt : TYPE = Pandas series
        Original series filtered for measurements outside the edges

    """   
    # correction1: if more than 30 cm out = nan
    ts.loc[ts <= edge1+0.3] = np.nan
    ts.loc[ts >= edge2+0.3] = np.nan    
    
    # correction2: if less than 30 outside = edge
    ts.loc[(ts > edge1-0.3) & (ts < edge1)] = edge1
    ts.loc[(ts < edge2+0.3) & (ts > edge2)] = edge2
       
    return ts

def filter_interp(t,x,y,win_med,gap_thres):
    """
    Filters the x,y data by linear interpolation across median filtered data
    Returns 'completed' time series between edges
    If gap larger than threshold: no imputation
    If gap larger than win_med/2: no median filter
    

    Parameters
    ----------
    t : TYPE = pandas Series
        time series of time values expressed in relative seconds
    x : TYPE = pandas Series
        time series of x values that needs imputation
    y : TYPE  = pandas Series
        time series of y values that needs imputation
    win_med : TYPE = Int/float
        Window for the median filter (kernel size)
    gap_thres : TYPE = Int/float
        gap threshold for when data imputation is not reliable anymore

    Returns
    -------
    df3 : TYPE = Pandas DataFrame
        DataFrame with imputed, filtered and completed data. From these,
        barn area and time budget allocations can be calculated.
    """

    # construct dataframe
    df = pd.DataFrame({'X' : x,
                       'y' : y,
                       't' : t})
    # calculate gaps from t
    df["gap"] = t.diff()
    df = df.reset_index(drop=1)
    
    # find gapsize < window/2 seconds -- only apply median filter to these
    gapind = df["gap"].loc[df["gap"]>round(win_med/2)].index #startindex
    gapind = pd.DataFrame(np.append(gapind,0))
    gapind = gapind.sort_values(by=0).reset_index(drop=1)
    gapind2 = pd.DataFrame(np.append(df["gap"].loc[df["gap"]>round(win_med/2)].index,len(df)-1)) #endindex
    gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)
    
    # prepare calculations
    df["xfilt"] = np.nan
    df["yfilt"] = np.nan
    
    # calculate median depending on gapsize
    for i in range(0,len(gapind)):
        print(gapind.iloc[i,0],gapind2.iloc[i,0])
        if gapind2.iloc[i,0]-gapind.iloc[i,0] > win_med:
            # median filter
            a = medfilt(df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = a
            a = medfilt(df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = a
        else:
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = \
                df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = \
                df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
    
    # linear (x,y) interpolation based on xfilt and yfilt
    tmin = df["t"].min()
    tmax = df["t"].max()
    tnew = np.linspace(tmin,tmax,tmax-tmin).round()
    
    # do the interpolation
    f = scipy.interpolate.interp1d(df["t"],[df["xfilt"],df["yfilt"]],kind="linear")
    xnew,ynew = f(tnew)
    
    # to dataframe
    df2 = pd.DataFrame({'t' : tnew,
                        'xnew' : xnew,
                        'ynew' : ynew})
    
    # merge with df based on key = t and tnew
    df3 = pd.merge(df2,df,
                   how = 'outer',
                   on = 't').sort_values(by="t").reset_index(drop=1)
    
    # find gapsize > gap_thres seconds -- to delete data
    gapind = df3["gap"].loc[df3["gap"]>gap_thres].index
    gapind2 = df3["gap"].loc[df3["gap"]>gap_thres]
    gaps = pd.DataFrame({'indx' : gapind,
                          'gapsize' : gapind2}).reset_index(drop=1)
        
    # delete xnew and ynew if gap > gap_thresh
    for i in range(0,len(gaps)):
        print(i)
        df3.loc[int(gaps["indx"][i]-gaps["gapsize"][i]+1): \
                int(gaps.indx[i])-1,["xnew","ynew"]] = np.nan    
    
    return df3, df3["xnew"], df3["ynew"]


#%% set filepaths and constants

# set path to data X,y
path_os = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set start and end dates
startdate = date(2022,5,1)
enddate = date(2022,5,31)
current_month = "may_"

# set path to excel file with cowids
path = os.path.join("W:","/ASG","WLR_Dataopslag","DairyCampus",
                    "3406_Nlas","raw","dc_sewio"
                    )

# set file name for cowid information
fn = "\Copy_of_Sewio_tags.xlsx"

# set path to write results to
path_res = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","cKamphuis","ddhtSP","data/"
                        )

#%% load data and edit for number of seconds data retained = nsec

# read position data
data = read_all_data(startdate,enddate,path_os)

# read id/tag data
tags = read_ids(path, fn)

# combine it with data
data = combine_data_id(data,tags)
data = data[["cowid","barn","date","at","feed_reference","title","X","y","alias"]]

# unique 'seconds' in the dataset 
data["day"] = data["at"].dt.day - min(data["at"].dt.day)
data["hour"] = data["at"].dt.hour
data["minute"] = data["at"].dt.minute
data["second"] = data["at"].dt.second
data["relsec"] = data["day"]*86400 \
                 + data["hour"]*3600 \
                 + data["minute"]*60 \
                 + data["second"]
nsec = data["relsec"]
cowids = data["cowid"]
df = pd.concat([cowids,nsec],axis = 1).drop_duplicates()  # gives the indices of unique first 
del df["relsec"], df["cowid"]

# innerjoin for selection step
data = pd.concat([data,df],axis = 1, join = 'inner')
data = data.sort_values(by = ["cowid","at"]).reset_index(drop=1)

# add relative time expressed in days
data["reltime"] = data["day"] + (data["hour"]*3600 +
                                 data["minute"]*60 +
                                 data["second"])/86400

# calculate gaps based on numeric time relative to start of dataset
data["gap"] = data["relsec"].diff()
data.loc[data["gap"] < 0,"gap"] = np.nan

# set data outside edges to edges x < 0 and < 10.6 or x < 21 and x > 32
data.loc[(data["X"] < 15),"X"] = filter_barn_edges( \
              copy.copy(data.loc[(data["X"] < 15),"X"]),0,10.6) #barn 72
data.loc[(data["X"] >= 15),"X"] = filter_barn_edges( \
              copy.copy(data.loc[(data["X"] >= 15),"X"]),21,32) # barn 70

# set data outside edges to edges y < -18 or y > -2.5
data["y"] = filter_barn_edges(copy.copy(data["y"]),-18,-2.5)

data["X"].hist()
data["y"].hist()

# clear workspace
del df, enddate, startdate, path, path_os, fn, tags, nsec, cowids 





#%% filter the data and save

# cows in the barn
cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
cows = pd.DataFrame(cows)

# days in the dataset
days = np.linspace(min(data["day"]),max(data["day"]),max(data["day"])-min(data["day"])+1)
days = pd.DataFrame(days)
days.columns = ["day"]

# concat
ncow = pd.concat([cows]*len(days)).sort_values(by='cowid').reset_index(drop=1)
nday = pd.concat([days]*len(cows)).reset_index(drop=1)

# cowdays
cowdays = pd.concat([ncow,nday], axis=1)

# loop over all data per cow-day and select data from this cow to write
win_med = 31
gap_thres = 180
for i in range(0,len(cowdays)):
    # select data
    cow_t = data.loc[(data["cowid"] == cowdays["cowid"][i]) & \
                     (data["day"] == int(cowdays["day"][i])),"relsec"]
    cow_x = data.loc[(data["cowid"] == cowdays["cowid"][i]) & \
                     (data["day"] == int(cowdays["day"][i])),"X"]
    cow_y = data.loc[(data["cowid"] == cowdays["cowid"][i]) & \
                     (data["day"] == int(cowdays["day"][i])),"y"]
    
    # filtered data
    if len(cow_t) > 10:

        # if t doesn't end at second 86399 of the day
        if (float((cow_t.iloc[-1]+1)//86400)) - ((cow_t.iloc[-1]+1)/86400)  > 0.000001:
            cow_t = pd.concat([cow_t,pd.Series((cow_t.iloc[-1]//86400+1)*86400-1)], axis=0)
            cow_x = pd.concat([cow_x,pd.Series(np.nan)], axis=0)
            cow_y = pd.concat([cow_y,pd.Series(np.nan)], axis=0)
        
        # if t doesn't start with 0th second of the day
        if cow_t.iloc[0]/86400 - float(cow_t.iloc[0]//86400) > 0.00001:
            cow_t = pd.concat([pd.Series(cow_t.iloc[0]//86400*86400),cow_t], axis=0)
            cow_x = pd.concat([pd.Series(np.nan),cow_x], axis=0)
            cow_y = pd.concat([pd.Series(np.nan),cow_y], axis=0)
        
        # filter
        df,x,y = filter_interp(cow_t,cow_x,cow_y,win_med,gap_thres)
        df = df.reset_index()
        
        # asign barn areas
        data_area, parea = barn_areas.assign_behaviour(x,y)
        data_area = data_area[~data_area.index.duplicated(keep='first')]
        
        # merge with df
        df["area"] = data_area["no"]
    else:
        df = pd.DataFrame({"t" : pd.Series(np.arange(0,86400,1)),
                           "xnew" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "ynew" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "X" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "y" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "gap" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "xfilt" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "yfilt" : pd.Series(np.arange(0,86400,1)*np.nan),
                           "area" : pd.Series(np.arange(0,86400,1)*np.nan)})

    # save results
    fn = 'data_' + current_month + str(cowdays["cowid"][i]) + '_' + str(int(cowdays["day"][i])) + '.txt'
    df.to_csv(path_res + fn)

    

#%% plots
    # plot result
    # fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
    # plt.subplot(2,1,1)
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("X [m]")
    # plt.plot(df["t"], df["X"],'o', lw = 3,color = 'mediumblue')
    # plt.plot(df["t"],df["xnew"],ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df["t"], df["xfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.subplot(2,1,2)
    # plt.plot(df["t"],df["y"],'o',lw = 3,color = 'mediumblue')
    # plt.plot(df["t"],df["ynew"],ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df["t"],df["yfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("y [m]")  




