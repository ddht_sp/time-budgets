# Time budgets

Time budget calculation based on cow location

## Project information

Created by Ines Adriaens  
18/05/2022

## Data

x,y position data from cows during 2 weeks for developement:
- 1/05 until 14/05


## Code modules

- load and edit data
- define locations
- functions for visualisation

## License (to be added)
For open source projects, say how it is licensed.

## Project status

Ongoing
