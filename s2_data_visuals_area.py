# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 13:36:55 2022

@author: adria036


------------------------------------------------------------------------------
KB sensing potential:
    
Module 2:
    - load packages
    - read data with barn areas
    - visualise data in time series

"""

#%% import packages

import os
import pandas as pd
# import numpy as np
# import copy
import matplotlib.pyplot as plt
# from datetime import date
#from read_data_sewio import read_all_data, read_ids, combine_data_id
#from scipy.signal import medfilt
#import scipy
#import timeit
#import barn_areas


#%% filepaths and constants

# set path to load data from
path = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","cKamphuis","ddhtSP","data/"
                        )

# results path
path_out = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","cKamphuis","ddhtSP","results/"
                        )

# set filenames 
fn = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f)) \
      and ".txt" in f] 
fn.sort()
fn = pd.DataFrame(fn,columns=["fn"])
test = fn['fn'].str.split(pat="_",expand = True)
test.columns = ["data","month","cowid","day"]
days = test['day'].str.split(pat=".",expand = True)
test["day"] = days[0]
test = test.drop("data",axis = 1)
months = pd.to_datetime(test["month"], format = "%b")
fn["month"] = months.dt.month
fn["cowid"] = test["cowid"].astype(int)
fn["day"] = test["day"].astype(int)+1
fn["year"]= 2022
fn = fn.sort_values(by=["cowid","year","month","day"]).reset_index(drop=1)
fn["date"] = pd.to_datetime({"year" : fn["year"],
                             "month" : fn["month"],
                             "day" : fn["day"]})
del test,months

#%% load data and analyse + save figure

for cow in fn["cowid"].drop_duplicates():
    print(cow)

    # load data
    data = pd.DataFrame([])
    for f in range(0,len(fn)):
        print(fn["fn"][f])
        # load data
        if fn["cowid"][f] == cow:
            try:
                temp = pd.read_csv(os.path.join(path,fn["fn"][f]),
                           usecols=("index","xnew","ynew","X","y","gap","xfilt","yfilt","area"))
                temp["t"] = temp["index"]
            except:
                temp = pd.read_csv(os.path.join(path,fn["fn"][f]),
                           usecols=("t","xnew","ynew","X","y","gap","xfilt","yfilt","area"))
                
            # add date
            temp["date"] = fn["date"][f]
            temp["cowid"] = fn["cowid"][f]
        
            # add to data
            data = pd.concat([data,temp],axis=0, ignore_index=1)
    
    data["t"] = data["t"].astype(int)
    data = data.sort_values(by=["date","t"]).reset_index(drop=1)
    
    data["datetime"] = data["t"].astype('timedelta64[s]') + data["date"]
    
    #%% visualise data
    
    # check data -> soeme datasets are incomplete
    fig = plt.subplots(nrows = 1, ncols = 1,figsize = (25,10))
    plt.subplot(1,1,1)
    plt.xlabel("date")
    plt.ylabel("t")
    plt.plot(data["date"],data["t"],'-', lw = 0.5,color = 'mediumblue')
    
    # check areas
    area = data[["date","area","t"]].groupby(["date","area"]).count()
    area = area.reset_index()
    area.columns = ["date","area","count"]
    test = area.groupby("date").sum()
    area["pcount"] = area["count"]/86400*100
    
    fig, ax = plt.subplots(1, figsize=(13, 12))
    # colors = ['#22577A', '#286A85','#2D7D90', '#38A3A5', '#57CC99','#80ED99','#C7F9CC']
    colors = ['#E07A5F', '#286A85','#2D7D90', '#38A3A5', '#F2CC8F','#80ED99','#C7F9CC']
    labels = ['vain','cub_a','cub_b','cub_c','feedrack','drink','conc']
    for i in range(0,7):
        print(i)
        x = area.loc[area["area"] == i,"date"].reset_index(drop=1)
        y = area.loc[area["area"] == i,"pcount"]
        if i > 0:
            l = area.loc[area["area"] < i,["area","date","pcount"]].groupby("date").sum()
            l = l.reset_index()
            l = pd.merge(x,l, how = 'outer')
            l = pd.merge(x,l,how= 'inner')
            l = l["pcount"].tolist()
        else:
            l = len(x)*[0]
            
        plt.barh(x,
                 y,
                 color=colors[i],
                 left = l
                 )
        
    # title, legend, labels
    plt.title('% location in areas per day of cow ' + str(cow), loc='center')
    plt.legend(labels, bbox_to_anchor=([1, 1, 0, 0]), ncol=7, frameon=False)
    plt.xlabel('% of area occupation')
    plt.ylabel("date")
    # adjust limits and draw grid lines
    plt.xlim(-0.5, ax.get_xticks()[-2] + 0.5)
    ax.set_axisbelow(True)
    ax.xaxis.grid(color='gray', linestyle='dashed')
    
    # save figure
    plt.savefig(path_out + 'areas_cow_' + str(cow) + '.png')



#TODO: time aspect in the budget groups