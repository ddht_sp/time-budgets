# -*- coding: utf-8 -*-
"""
Created on Mon May 23 13:22:02 2022

@author: adria036
"""





import numpy as np
import pandas as pd


def non_uniform_savgol(x, y, window, polynom):
    """
    Applies a Savitzky-Golay filter to y with non-uniform spacing
    as defined in x

    This is based on https://dsp.stackexchange.com/questions/1676/savitzky-golay-smoothing-filter-for-not-equally-spaced-data
    The borders are interpolated like scipy.signal.savgol_filter would do

    Parameters
    ----------
    x : array_like
        List of floats representing the x values of the data
    y : array_like
        List of floats representing the y values. Must have same length
        as x
    window : int (odd)
        Window length of datapoints. Must be odd and smaller than x
    polynom : int
        The order of polynom used. Must be smaller than the window size

    Returns
    -------
    np.array of float
        The smoothed y values
    """
    if len(x) != len(y):
        raise ValueError('"x" and "y" must be of the same size')

    if len(x) < window:
        raise ValueError('The data size must be larger than the window size')

    if type(window) is not int:
        raise TypeError('"window" must be an integer')

    if window % 2 == 0:
        raise ValueError('The "window" must be an odd integer')

    if type(polynom) is not int:
        raise TypeError('"polynom" must be an integer')

    if polynom >= window:
        raise ValueError('"polynom" must be less than "window"')

    half_window = window // 2
    polynom += 1

    # Initialize variables
    A = np.empty((window, polynom))     # Matrix
    tA = np.empty((polynom, window))    # Transposed matrix
    t = np.empty(window)                # Local x variables
    y_smoothed = np.full(len(y), np.nan)

    # Start smoothing
    N = 0
    for i in range(half_window, len(x) - half_window, 1):
        # print(i)
        N += 1
        
        # Center a window of x values on x[i]
        for j in range(0, window, 1):
            #print(j)
            t[j] = x[i + j - half_window] - x[i]
            
            #NEW: t2 with stepsize 1
            t2 = list(range(x[i - half_window] - x[i], x[i + (window-1) - half_window] - x[i]+1))
            t2 = np.asarray(t2,dtype = float)
        
        #NEW: Initialize variables based on variable window
        A2 = np.empty((len(t2), polynom))     # Matrix
        # tA2 = np.empty((polynom, len(t2)))    # Transposed matrix
            
        # Create the initial matrix A and its transposed form tA
        for j in range(0, window, 1):
            r = 1.0
            for k in range(0, polynom, 1):
                A[j, k] = r
                tA[k, j] = r
                r *= t[j]
                
        for j in range(0,len(t2),1):
            # print(j,t2[j])
            r = 1.0
            for k in range(0, polynom, 1):
                A2[j, k] = r
                # tA2[k, j] = r
                r *= t2[j]

        # Multiply the two matrices = (A^T*A)
        tAA = np.matmul(tA, A)

        # Invert the product of the matrices =(A^T*A)-1
        tAA = np.linalg.inv(tAA)

        # Calculate the pseudoinverse of the design matrix = (A^T*A)-1*A^T 
        coeffs = np.matmul(tAA, tA)
        
        #NEW: Calculate c by matrix multiplying it with the original known y values
        #NEW: in the window of interest
        c = np.matmul(coeffs,y[i-half_window:i+window-1-half_window+1])
        
        #NEW: c[0] = is the smoothed value for i
        #NEW: we can calculate the interpolated values using y = A2*c
        y_interp = np.matmul(A2,c)
        
        #NEW: rearrange to return - select the values needed for the smoothing
        #NEW: based on the frames that we have sampled
        if N == 1:
            new_data = pd.DataFrame(t2,columns = ["tstep"])
            new_data["frameno"] = new_data["tstep"] + x[i]
            new_data["y_smooth"+str(N)] = y_interp
            new_data = new_data.drop("tstep",axis = 1)
        else:
            sm_data = pd.DataFrame(t2,columns = ["tstep"])
            sm_data["frameno"] = sm_data["tstep"] + x[i]
            sm_data["y_smooth"+str(N)] = y_interp
            sm_data = sm_data.drop("tstep", axis = 1)
            new_data = new_data.merge(sm_data,how='outer',on='frameno')
                
        # Calculate c0 which is also the y value for y[i] based on available only
        y_smoothed[i] = 0
        for j in range(0, window, 1):
            y_smoothed[i] += coeffs[0, j] * y[i + j - half_window]
            
        # If at the end or beginning, store all coefficients for the polynom
        if i == half_window:
            first_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    first_coeffs[k] += coeffs[k, j] * y[j]
        elif i == len(x) - half_window - 1:
            last_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    last_coeffs[k] += coeffs[k, j] * y[len(y) - window + j]

    # Interpolate the result at the left border
    for i in range(0, half_window, 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += first_coeffs[j] * x_i
            x_i *= x[i] - x[half_window]

    # Interpolate the result at the right border
    for i in range(len(x) - half_window, len(x), 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += last_coeffs[j] * x_i
            x_i *= x[i] - x[half_window - 1]
            
    #NEW: compute interpolated mean to sample when not available
    new_data["mean"] = new_data.iloc[:,1:].mean(axis=1)
    y_all = new_data[["frameno","mean"]]

    return y_smoothed, y_all
