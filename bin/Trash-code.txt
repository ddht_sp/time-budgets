# -*- coding: utf-8 -*-
"""
Created on Mon May 30 19:02:56 2022

@author: adria036

------------------------------------
Bin of s1_data_edit

"""


# def filter_interp(t,x,y,win_med):
#     """
#     Filters and interpolates the (x,y) coordinates:
#         1) median filter
#         2) linear interpolation of the (x,y)-values
#     !! apply this function on pieces of data with gaps < 30s for it to be sensible    

#     Parameters
#     ----------
#     t : TYPE = Pandas series
#         Contains the time stamp, expressed in seconds
#     x : TYPE = Pandas series
#         Contains the x coordinate, expressed in meters
#     y : TYPE = Pandas series
#         Contains the y coordinate, expressed in meters
#     win_med : TYPE = Int
#         Window size for median filter

#     Returns
#     -------
#     tnew :
#         interpolated time array (all seconds, no gaps)
#     xfilt : 
#         median filtered x data, size = size x
#     yfilt :
#         median filtered y data, size = size y
#     xfilt_new :
#         interpolated x data, size = tnew
#     yfilt_new :
#         interpolated y data, size = tnew
#     """
#     #--------------------------------------------------------------------------
#     # for development only (delete)
#     strt = 10000
#     endt = 10400
#     x = data["X"].loc[strt:endt] 
#     t = data["relsec"].loc[strt:endt]
#     y = data["y"].loc[strt:endt]
#     win_med = 31
#     #--------------------------------------------------------------------------
    
#     # medan filtered x and y
#     xfilt = medfilt(x,kernel_size = win_med)
#     yfilt = medfilt(y,kernel_size = win_med)
    
#     # set values to interpolate
#     tmin = t.min()
#     tmax = t.max()
#     tnew = np.linspace(tmin,tmax,tmax-tmin).round()
    
#     # do the interpolation
#     f = scipy.interpolate.interp1d(t,[xfilt,yfilt],kind="linear")
#     xnew,ynew = f(tnew)
    
#     fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
#     plt.subplot(2,1,1)
#     plt.xlabel("numeric time [s]")
#     plt.ylabel("X [m]")
#     plt.plot(t,x,'o-', lw = 3,color = 'mediumblue')
#     plt.plot(tnew,xnew,ls = "-",marker = 's',color = 'seagreen')
#     plt.plot(t,xfilt,ls = ':', marker = '*',lw=2, color = 'orangered')
#     plt.subplot(2,1,2)
#     plt.plot(t,y,'o-',lw = 3,color = 'mediumblue')
#     plt.plot(tnew,ynew,ls = "-",marker = 's',color = 'seagreen')
#     plt.plot(t,yfilt,ls = ':', marker = '*',lw=2, color = 'orangered')
#     plt.xlabel("numeric time [s]")
#     plt.ylabel("y [m]")
    
    
#     return tnew, xfilt, yfilt, xfilt_new, yfilt_new


    
    #--------------------------------------------------------------------------
    # # for development only (delete)
    # strt = 5000
    # endt = 5400
    # x = data["X"].loc[strt:endt] 
    # t = data["relsec"].loc[strt:endt]
    # y = data["y"].loc[strt:endt]
    # win_med = 31
    # gap_thres = 20
    #--------------------------------------------------------------------------
    
    # # plot result
    # fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
    # plt.subplot(2,1,1)
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("X [m]")
    # plt.plot(df["t"], df["X"],'o-', lw = 3,color = 'mediumblue')
    # #plt.plot(tnew,xnew,ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df["t"], df["xfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.subplot(2,1,2)
    # plt.plot(df["t"],df["y"],'o-',lw = 3,color = 'mediumblue')
    # #plt.plot(tnew,ynew,ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df["t"],df["yfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("y [m]")
     
    
    # # plot result
    # fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
    # plt.subplot(2,1,1)
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("X [m]")
    # plt.plot(df3["t"], df3["X"],'o', lw = 3,color = 'mediumblue')
    # plt.plot(df3["t"],df3["xnew"],ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df3["t"], df3["xfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.subplot(2,1,2)
    # plt.plot(df3["t"],df3["y"],'o',lw = 3,color = 'mediumblue')
    # plt.plot(df3["t"],df3["ynew"],ls = "-",marker = 's',color = 'seagreen')
    # plt.plot(df3["t"],df3["yfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    # plt.xlabel("numeric time [s]")
    # plt.ylabel("y [m]")          
    
    
    #%% filter the data


    test = data.loc[(data["cowid"] == 427) & (np.floor(data["reltime"]) == 1),:]
    test = test.reset_index(drop=1)

    # find gapsize < 60 seconds -- only apply median filter to these
    gapind = test.loc[test["gap"]>60].index
    gapind = pd.DataFrame(np.append(gapind,0))
    gapind = gapind.sort_values(by=0).reset_index(drop=1)
    gapind2 = pd.DataFrame(np.append(test.loc[test["gap"]>60].index,len(test)-1))
    gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)

    test["xfilt"] = np.nan
    test["yfilt"] = np.nan
    intdatax = []
    intdatay = []
    relsec = []
    for i in range(0,len(gapind)):
        print(gapind.iloc[i,0],gapind2.iloc[i,0])
        if gapind2.iloc[i,0]-gapind.iloc[i,0] > 31:
            # median filter
            a = medfilt(test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
            test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
            a = medfilt(test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
            test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
            # interpolate on median value
            xmin = test["relsec"].loc[gapind.iloc[i,0]]
            xmax = test["relsec"].loc[gapind2.iloc[i,0]]
            xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            x = np.linspace(xmin, xmax, xmax-xmin)
            # for x
            yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
            y_nearest = interp(xi)
            intdatax = np.append(intdatax,y_nearest)
            # for y
            yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
            y_nearest = interp(xi)
            intdatay = np.append(intdatay,y_nearest)
        else:
            test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
                test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
                test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            
            # interpolate on median value
            xmin = test["relsec"].loc[gapind.iloc[i,0]]
            xmax = test["relsec"].loc[gapind2.iloc[i,0]]
            xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            x = np.linspace(xmin, xmax, xmax-xmin)
            # for x
            yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
            y_nearest = interp(xi)
            intdatax = np.append(intdatax,y_nearest)
            # for y
            yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
            y_nearest = interp(xi)
            intdatay = np.append(intdatay,y_nearest)
                   
            relsec = np.append(relsec,x)
                
           



    #%timeit test["xfilt"] = medfilt(test["X"],kernel_size = [31])
    #%timeit test["yfilt"] = medfilt(test["y"],kernel_size = [31])

    # visualise filtered data
    fig,ax = plt.subplots(ncols = 1, nrows = 2, figsize = (20,10))
    plt.subplot(2,1,1)
    plt.plot(test["reltime"],
                     test["X"],
                     color = "indigo",
                     lw = 2)
    plt.plot(test["reltime"],
                     test["xfilt"],
                     color = "darkturquoise",
                     ls = "-")
    plt.xlabel("relative time")
    plt.ylabel("x")

    plt.subplot(2,1,2)
    ax[1] = plt.plot(test["reltime"],
                     test["y"],
                     color = "indigo",
                     lw = 2)
    ax[1] = plt.plot(test["reltime"],
                     test["yfilt"],
                     color = "darkturquoise",
                     ls = "-")
    plt.xlabel("relative time")
    plt.ylabel("y")

    # visualise interpolated data (in relsec, intdatax and intdatay)


    # visualise gaps
    fig,ax = plt.subplots(ncols = 2, nrows = 1, figsize = (20,10))
    plt.subplot(1,2,1)
    plt.grid(visible = True,axis = 'both')
    plt.hist(test.loc[test["gap"]<600,["gap"]], color = "darkred")
    plt.ylim(0, 16000)
    plt.xlim(0,600)
    plt.xlabel("gapsize [s]")
    plt.ylabel("No. of gaps")
    plt.title("gaps < 600s")
    plt.subplot(1,2,2)
    plt.grid(visible = True,axis = 'both')
    plt.hist(test.loc[test["gap"]<120,["gap"]], bins = 48,color = "darkred")
    plt.plot([31,31],[0,16000], linewidth=2, color = "darkblue")
    plt.ylim(0, 15000)
    plt.xlim(0,121)
    plt.xlabel("gapsize [s]")
    plt.ylabel("No. of gaps")
    plt.title("gaps < 120s")


    fig,ax = plt.subplots(ncols = 1, nrows = 2, figsize = (20,10))
    plt.subplot(2,1,1)
    plt.plot(test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]-2],
             test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]-2])
    plt.plot(test["relsec"].loc[gapind.iloc[i,0]+8:gapind2.iloc[i,0]-8],
             ysm[7:-7])
    plt.xlabel("relative time [s]")
    plt.ylabel("smoothed & filtered x")

    plt.subplot(2,1,2)
    plt.plot(test["relsec"].loc[gapind.iloc[i,0]+8:gapind2.iloc[i,0]-8],
              ysm[7:-7], lw = 2, color = "orange")
    plt.plot(yall["frameno"],
             yall["mean"],
             color = "darkred")
    plt.xlabel("relative time [s]")
    plt.ylabel("interpolated filtered x")
    
    #%% visualise and quality check
    """
        - amount of data points without the cowid/tag linked (OK)
        - gapsize
        - summary stats per animal + missing data visualisations
        - to be added
    """

    # data for which no cowid is available = 0
    nocow = data.loc[data["cowid"]==0 & data["X"].isnull(),:]
    nocow1 = data.loc[data["cowid"]==0,:]
    print("Number of datapoints without cowid and equal to NaN = " + str(len(nocow)))
    print("This is " + str(len(nocow)/len(nocow1)*100) + "% of the records with no cowid")

    # delete these rows
    data = data.loc[data["cowid"]>0,:]
    del nocow, nocow1

    # cows in the barn
    cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
    cows = pd.DataFrame(cows)

    # cowdays in the barn
    cowday = data[["cowid","date"]].drop_duplicates().sort_values(by = ["cowid","date"]).reset_index(drop=1)

    #TODO: need to check what to do when for a cow data are not available --> ignore?


    # summarize gaps and missing data (and relate this to the location if possible)



    # TODO: gap visualisations before filtering/data imputation > most gaps 1-5 seconds
    # TODO: FIRST median smooth X and Y separately

    # TODO: THEN data imputation with Savitsky-Golay function previously implemented
    data["gap"].hist()
    data.loc[(data["gap"] < 60) & (data["gap"] > 1),"gap"].hist()
    data.loc[(data["gap"] < 60) & (data["gap"] > 5),"gap"].hist()

    # number of datapoints and missing data (for selection purposes)
    counts = data.groupby(by = ["cowid","date"]).count()
    cowday["count"] = counts["barn"]


    cows[""]